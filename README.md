
### What is this repository for? ###

* This repo has source code for project named My-College. In this project you will get info about my college. E.g: - Facility,About,Events etc.
* This app is based on Cordova.
* For demo purpose an apk has been attached as well.


### How do I get set up? ###

* Install Node js 
* Then install cordova using npm.
	If you are on Mac/Linux use sudo npm install -g cordova
	If your are on window use npm install -g cordova
* Make sure you had installed and configure all dependencies require for cordova. e.g: Java Jdk, Android Sdk etc.
* for check current set of platform
	use cordova platform ls

### How do I run project? ###

* Go inside the folder named mycollege
* To build project for android use cordova build android
* To rebuild and view app in emulator use cordova emulate android

### Who do I talk? ###

* Repo owner is Kiranjeet Kaur. You can connect on kiranjeetk584@gmail.com

### Note :- ###
* License has been used with this repo. So, before use please read license file carefully.